pragma solidity ^0.4.21;


contract Retailer {
    address public creator;

    string public vatNumber;
    string public businessName;
    string public blocation;
    string public postalCode;

    string[] public orders;

    function Retailer(
        string vnumber,
        string bname,
        string location,
        string pcode
    ) public
    {
        creator = msg.sender;

        vatNumber = vnumber;
        businessName = bname;
        blocation = location;
        postalCode = pcode;

        return;
    }

    function addOrder(string newOrder) public {
        orders.push(newOrder);
    }

    function getCountOrders() public constant returns (uint) {
        return orders.length;
    }

    function getOrder(uint index) public constant returns (string) {
        return orders[index];
    }
}
